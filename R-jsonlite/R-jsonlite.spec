%global packname  jsonlite
%global rlibdir  %{_libdir}/R/library

# Several hard-require this package or are not yet available.
%global with_suggests 0

Name:             R-%{packname}
Version:          1.5
Release:          4%{?dist}
Summary:          A Robust, High Performance JSON Parser and Generator for R

# Bundled yajl is ISC.
License:          MIT and ISC
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:   R-methods
# Imports:
# Suggests:  R-httr R-curl R-plyr R-testthat R-knitr R-rmarkdown R-R.rsp R-sp
# LinkingTo:
# Enhances:

Requires:         R-methods
BuildRequires:    R-devel tex(latex) R-methods
BuildRequires:    R-testthat R-plyr R-sp
%if 0%{with_suggests}
Requires:         R-httr R-curl R-knitr R-rmarkdown R-R.rsp
BuildRequires:    R-httr R-curl R-knitr R-rmarkdown R-R.rsp
%endif
# https://github.com/jeroen/jsonlite/issues/201
Provides: bundled(yajl) = 2.1.1

%description
A fast JSON parser and generator optimized for statistical data and the web.
The package offers flexible, robust, high performance tools for working with
JSON in R and is particularly powerful for building pipelines and interacting
with a web API. In addition to converting JSON data from/to R objects,
'jsonlite' contains functions to stream, validate, and prettify JSON data.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_suggests}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname} --ignore-vignettes --no-examples
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%doc %{rlibdir}/%{packname}/CITATION
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%dir %{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/libs/%{packname}.so


%changelog
* Tue Sep 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.5-4
- Make note of bundled yajl.

* Thu Sep 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.5-3
- Skip testing examples.

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.5-2
- new package built with tito

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.5-1
- initial package for Fedora
