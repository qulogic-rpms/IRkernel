%global packname  repr
%global rlibdir  %{_datadir}/R/library


Name:             R-%{packname}
Version:          0.12.0
Release:          1%{?dist}
Summary:          Serializable Representations

License:          GPLv3
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-utils R-grDevices
# Suggests:  R-methods R-highr R-Cairo R-testthat
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core


Requires:         R-utils R-grDevices
Requires:         R-methods R-highr R-Cairo R-testthat
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-utils R-grDevices
BuildRequires:   R-methods R-highr R-Cairo R-testthat

%description
String and binary representations of objects for several formats / mime

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%{_bindir}/R CMD check %{packname}

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help


%changelog
* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.0-1
- initial package for Fedora
