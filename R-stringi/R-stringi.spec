%global packname  stringi
%global rlibdir  %{_libdir}/R/library


Name:             R-%{packname}
Version:          1.1.5
Release:          1%{?dist}
Summary:          Character String Processing Facilities

License:          BSD
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-tools R-utils R-stats
# Suggests:
# LinkingTo:
# Enhances:

Requires:         R-tools R-utils R-stats

BuildRequires:    R-devel tex(latex)
BuildRequires:    R-tools R-utils R-stats
BuildRequires:    libicu-devel


%description
Allows for fast, correct, consistent, portable, as well as convenient
character string/text processing in every locale and any native encoding.
Owing to the use of the ICU library, the package provides R users with
platform-independent functions known to Java, Perl, Python, PHP, and Ruby
programmers. Available features include: pattern searching (e.g., with ICU
Java-like regular expressions or the Unicode Collation Algorithm), random
string generation, case mapping, string transliteration, concatenation,
Unicode normalization, date-time formatting and parsing, etc.

%package devel
Summary:          Development files for %{name}
Requires:         %{name}%{?_isa} = %{version}-%{release}
%description devel
Development files for %{name}.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname} \
    --configure-args="--disable-icu-bundle"
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%{_bindir}/R CMD check %{packname}

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%doc %{rlibdir}/%{packname}/AUTHORS
%doc %{rlibdir}/%{packname}/CITATION
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/libs

%files devel
%{rlibdir}/%{packname}/include

%changelog
* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.5-1
- initial package for Fedora
