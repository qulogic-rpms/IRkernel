%global packname  withr
%global rlibdir  %{_datadir}/R/library


Name:             R-%{packname}
Version:          2.0.0
Release:          2%{?dist}
Summary:          Run Code 'With' Temporarily Modified Global State

License:          GPLv2+
URL:              http://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          http://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-stats R-graphics
# Suggests:  R-testthat
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core


Requires:         R-stats R-graphics
Requires:         R-testthat
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-stats R-graphics
BuildRequires:    R-testthat

%description
A set of functions to run code 'with' safely and temporarily modified
global state. Many of these functions were originally a part of the
'devtools' package, this provides a simple package with limited
dependencies to provide access to these functions.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%{_bindir}/R CMD check %{packname}

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS.md
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help


%changelog
* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.0.0-2
- Clean up old stuff in spec.

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.0.0-1
- Update to latest release.

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.0.0-1
- New upstream release

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-1
- initial package for Fedora
