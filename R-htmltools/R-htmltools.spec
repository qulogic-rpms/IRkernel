%global packname  htmltools
%global rlibdir  %{_libdir}/R/library


Name:             R-%{packname}
Version:          0.3.6
Release:          1%{?dist}
Summary:          Tools for HTML

License:          GPLv2+
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-utils R-digest R-Rcpp
# Suggests:  R-markdown R-testthat
# LinkingTo:
# Enhances:

Requires:         R-utils R-digest R-Rcpp
Requires:         R-markdown R-testthat
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-utils R-digest R-Rcpp-devel
BuildRequires:    R-markdown R-testthat

%description
Tools for HTML generation and output.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
export LANG=C.UTF-8
%{_bindir}/R CMD check %{packname}

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/libs


%changelog
* Fri Aug 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.6-1
- initial package for Fedora
