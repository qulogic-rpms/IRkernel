%global packname  stringr
%global rlibdir  %{_datadir}/R/library

# Both knitr and rmarkdown are required for docs, but require this package.
%global with_doc  0

Name:             R-%{packname}
Version:          1.2.0
Release:          1%{?dist}
Summary:          Simple, Consistent Wrappers for Common String Operations

License:          GPLv2
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-stringi R-magrittr
# Suggests:  R-testthat R-knitr R-htmltools R-htmlwidgets R-rmarkdown R-covr
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core

# NOTE: Don't really need R-covr for coverage.
Requires:         R-stringi >= 0.4.1
Requires:         R-magrittr
Requires:         R-testthat R-htmltools R-htmlwidgets
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-stringi >= 0.4.1
BuildRequires:    R-magrittr
BuildRequires:    R-testthat R-htmltools R-htmlwidgets
%if 0%{with_doc}
BuildRequires:    R-knitr R-rmarkdown
%endif

%description
A consistent, simple and easy to use set of wrappers around the fantastic
'stringi' package. All function and argument names (and positions) are
consistent, all functions deal with "NA"'s and zero length vectors in the
same way, and the output from one function is easy to feed into the input
of another.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_doc}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname} --ignore-vignettes
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%license %{rlibdir}/%{packname}/LICENSE
%doc %{rlibdir}/%{packname}/NEWS.md
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/data
%{rlibdir}/%{packname}/htmlwidgets


%changelog
* Tue Sep 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.0-1
- new package built with tito

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.0-1
- initial package for Fedora
