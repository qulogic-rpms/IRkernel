%global packname  yaml
%global rlibdir  %{_libdir}/R/library


Name:             R-%{packname}
Version:          2.1.14
Release:          2%{?dist}
Summary:          Methods to Convert R Data to YAML and Back

License:          BSD
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:
# Suggests:  R-testthat
# LinkingTo:
# Enhances:




Requires:         R-testthat
BuildRequires:    R-devel tex(latex)

BuildRequires:   R-testthat

%description
Implements the 'libyaml' 'YAML' 1.1 parser and emitter
(<http://pyyaml.org/wiki/LibYAML>) for R.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css
rm %{buildroot}%{rlibdir}/%{packname}/implicit.re

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/tests
%{rlibdir}/%{packname}/CHANGELOG
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/THANKS
%{rlibdir}/%{packname}/help


%changelog
* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.1.14-2
- Fix license field.

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.1.14-1
- initial package for Fedora
