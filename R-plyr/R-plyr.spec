%global packname  plyr
%global rlibdir  %{_libdir}/R/library

# Prevent loops and heavy dependencies.
%global with_suggests 0

Name:             R-%{packname}
Version:          1.8.4
Release:          4%{?dist}
Summary:          Tools for Splitting, Applying and Combining Data

License:          MIT
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-Rcpp
# Suggests:  R-abind R-testthat R-tcltk R-foreach R-doParallel R-itertools R-iterators R-covr
# LinkingTo:
# Enhances:

BuildRequires:    R-devel tex(latex)
BuildRequires:    R-Rcpp-devel R-abind R-testthat
Requires:         R-Rcpp
%if 0%{with_suggests}
Requires:         R-tcltk R-foreach R-doParallel R-itertools R-iterators R-covr
BuildRequires:    R-tcltk R-foreach R-doParallel R-itertools R-iterators R-covr
%endif

%description
A set of tools that solves a common set of problems: you need to break a
big problem down into manageable pieces, operate on each piece and then
put all the pieces back together.  For example, you might want to fit a
model to each spatial location or time point in your study, summarise data
by panels or collapse high-dimensional arrays to simpler summary
statistics. The development of 'plyr' has been generously supported by
'Becton Dickinson'.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_suggests}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname}
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/CITATION
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/data
%{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/libs/%{packname}.so


%changelog
* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.8.4-4
- Remove extra rm from install step.

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.8.4-3
- Re-add Rcpp runtime dependency.

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.8.4-2
- new package built with tito

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.8.4-1
- initial package for Fedora
