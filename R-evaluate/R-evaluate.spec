%global packname  evaluate
%global rlibdir  %{_datadir}/R/library

# Heavy dependencies.
%global with_suggests 0

Name:             R-%{packname}
Version:          0.10.1
Release:          4%{?dist}
Summary:          Parsing and Evaluation Tools that Provide More Details than the Default

License:          MIT
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-methods R-stringr
# Suggests:  R-testthat R-lattice R-ggplot2
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core


Requires:         R-methods R-stringr
Suggests:         R-testthat R-lattice R-ggplot2
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-methods R-stringr
BuildRequires:   R-testthat R-lattice

%if %{with_suggests}
BuildRequires:   R-ggplot2
%endif

%description
Parsing and evaluation tools that make it easy to recreate the command
line behaviour of R.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_suggests}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname}
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%license %{rlibdir}/%{packname}/LICENSE
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help


%changelog
* Sun Oct 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.1-4
- Fix file list.

* Sun Oct 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.1-3
- Remove extra file.

* Sun Oct 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.1-2
- Don't BR ggplot2 yet.

* Sat Oct 28 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.1-1
- initial package for Fedora
