%global packname  pbdRPC
%global rlibdir  %{_datadir}/R/library


Name:             R-%{packname}
Version:          0.1.1
Release:          7%{?dist}
Summary:          Programming with Big Data -- Remote Procedure Call

# NOTE: There is a bundled copy of putty which is MIT, but we disable its usage
# in favor of ssh.
License:          MPLv2.0
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_0.1-1.tar.gz
# https://github.com/snoweye/pbdRPC/pull/7
Patch0001:        fix-configure.patch

# Here's the R view of the dependencies world:
# Depends:
# Imports:
# Suggests:
# LinkingTo:
# Enhances:

BuildArch:        noarch
BuildRequires:    R-devel tex(latex) 
BuildRequires:    openssh-clients

Requires:         R-core
Requires:         openssh-clients

%description
A very light implementation yet secure for remote procedure calls with
unified interface via ssh (OpenSSH).

%prep
%setup -q -c -n %{packname}
pushd %{packname}
%patch0001 -p1
popd

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname} --configure-args="--disable-plink"
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css
# Remove unused license (it's installed even if option is disabled.)
rm %{buildroot}%{rlibdir}/%{packname}/putty_LICENCE

%check
%{_bindir}/R CMD check %{packname}

%files
%license %{packname}/COPYING
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%doc %{rlibdir}/%{packname}/CITATION
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help


%changelog
* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-7
- Fix path to license file.

* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-6
- Add license file to rpm.

* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-5
- Add R-core Require for noarch package.

* Fri Aug 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-4
- Install to correct noarch directory, but simpler.

* Fri Aug 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-3
- Install into correct noarch directory.

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-2
- Fix configuration options.

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.1.1-1
- initial package for Fedora
