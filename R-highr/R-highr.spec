%global packname  highr
%global rlibdir  %{_datadir}/R/library

# Needs knitr (build loop)
%global with_doc 0


Name:             R-%{packname}
Version:          0.6
Release:          2%{?dist}
Summary:          Syntax Highlighting for R Source Code

License:          GPLv2+
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:
# Suggests:  R-knitr R-testit
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core

BuildRequires:    R-devel tex(latex)
%if 0%{?with_doc}
Requires:         R-knitr R-testit
BuildRequires:    R-knitr R-testit
%endif


%description
Provides syntax highlighting for R source code. Currently it supports
LaTeX and HTML output. Source code of other languages is supported via
Andre Simon's highlight package (http://www.andre-simon.de).

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if 0%{?with_doc}
%{_bindir}/R CMD check %{packname}
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help


%changelog
* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.6-2
- Fix license and use https links.

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.6-1
- initial package for Fedora
