%global packname  htmlwidgets
%global rlibdir  %{_datadir}/R/library

# Remove knitr dependency, which also Suggests this package.
%global with_doc 0

Name:             R-%{packname}
Version:          0.9
Release:          2%{?dist}
Summary:          HTML Widgets for R

License:          MIT
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-htmltools R-jsonlite R-yaml
# Suggests:  R-knitr
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core

Requires:         R-htmltools R-jsonlite R-yaml
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-htmltools R-jsonlite R-yaml

Suggests:         R-knitr
%if %{with_doc}
BuildRequires:    R-knitr
%endif

%description
A framework for creating HTML widgets that render in various contexts including
the R console, 'R Markdown' documents, and 'Shiny' web applications.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_doc}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname} --ignore-vignettes
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/templates
%{rlibdir}/%{packname}/www


%changelog
* Thu Sep 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9-2
- Fix file list.
- Fix inverted condition.

* Fri Aug 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9-1
- initial package for Fedora
