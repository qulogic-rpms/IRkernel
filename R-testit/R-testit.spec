%global packname  testit
%global rlibdir  %{_datadir}/R/library

# Heavy dependencies.
%global with_suggests 0

Name:             R-%{packname}
Version:          0.7
Release:          1%{?dist}
Summary:          A Simple Package for Testing R Packages

License:          GPL
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:   
# Imports:   
# Suggests:  R-rstudioapi
# LinkingTo:
# Enhances:

BuildArch:        noarch

Suggests:         R-rstudioapi
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-devel

%if %{with_suggests}
BuildRequires:   R-rstudioapi
%endif

%description
Provides two convenience functions assert() and test_pkg() to facilitate
testing R packages.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_suggests}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname}
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/rstudio


%changelog
* Tue Sep 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.7-1
- new package built with tito

* Thu Sep 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.7-1
- initial package for Fedora
