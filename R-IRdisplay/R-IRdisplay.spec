%global packname  IRdisplay
%global rlibdir  %{_datadir}/R/library


Name:             R-%{packname}
Version:          0.4.4
Release:          1%{?dist}
Summary:          'Jupyter' Display Machinery

License:          MIT
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-repr
# Suggests:  R-testthat R-withr
# LinkingTo:
# Enhances:

BuildArch:        noarch
Requires:         R-core

Requires:         R-repr
Requires:         R-testthat R-withr
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-repr
BuildRequires:    R-testthat R-withr

%description
An interface to the rich display capabilities of 'Jupyter' front-ends
(e.g. 'Jupyter Notebook') <https://jupyter.org>. Designed to be used from
a running 'IRkernel' session <https://irkernel.github.io>.

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%{_bindir}/R CMD check %{packname}

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help


%changelog
* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.4-1
- initial package for Fedora
