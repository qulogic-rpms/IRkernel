%global packname  pbdZMQ
%global rlibdir  %{_libdir}/R/library


Name:             R-%{packname}
Version:          0.2.6
Release:          3%{?dist}
Summary:          Programming with Big Data -- Interface to ZeroMQ

License:          GPLv3
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_0.2-6.tar.gz
# https://github.com/snoweye/pbdZMQ/pull/38
Patch0001:        fix-configure.patch
# https://github.com/snoweye/pbdZMQ/pull/39
Patch0002:        fix-buffer-overflow.patch

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-R6
# Suggests:  R-pbdRPC
# LinkingTo:
# Enhances:

Requires:         R-R6
Requires:         R-pbdRPC
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-R6
BuildRequires:    R-pbdRPC
BuildRequires:    zeromq-devel >= 4.0.4

%description
'ZeroMQ' is a well-known library for high-performance asynchronous
messaging in scalable, distributed applications.  This package provides
high level R wrapper functions to easily utilize 'ZeroMQ'. We mainly focus
on interactive client/server programming frameworks.

%prep
%setup -q -c -n %{packname}
pushd %{packname}
%patch0001 -p1
%patch0002 -p1
popd

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname} \
    --configure-args="--disable-internal-zmq"
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css
# ZeroMQ copyright is installed even though internal build is disabled.
rm -r %{buildroot}%{rlibdir}/%{packname}/zmq_copyright

%check
%{_bindir}/R CMD check %{packname}

%files
%license %{packname}/COPYING
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%doc %{rlibdir}/%{packname}/CITATION
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/demo
%dir %{rlibdir}/%{packname}/etc
%{rlibdir}/%{packname}/etc/Makeconf
%{rlibdir}/%{packname}/examples
%{rlibdir}/%{packname}/libs


%changelog
* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.6-3
- Fix path to license file.

* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.6-2
- Add explicit directory listings.
- Add license file to rpm.

* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.6-1
- initial package for Fedora
