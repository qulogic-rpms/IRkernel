%global packname  sp
%global rlibdir  %{_libdir}/R/library

# Limit loops and extra dependencies.
%global with_suggests 0
# rgeos requires sp; this only suggests it.
%global with_loop 0

Name:             R-%{packname}
Version:          1.2.5
Release:          3%{?dist}
Summary:          Classes and Methods for Spatial Data

License:          GPLv2+
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_1.2-5.tar.gz

# Here's the R view of the dependencies world:
# Depends:   R-methods
# Imports:   R-utils R-stats R-graphics R-grDevices R-lattice R-grid
# Suggests:  R-RColorBrewer R-rgdal R-rgeos R-gstat R-maptools R-deldir
# LinkingTo:
# Enhances:


Requires:         R-methods
Requires:         R-utils R-stats R-graphics R-grDevices R-lattice R-grid
BuildRequires:    R-devel tex(latex) R-methods
BuildRequires:    R-utils R-stats R-graphics R-grDevices R-lattice R-grid
%if 0%{with_suggests}
Requires:         R-RColorBrewer R-rgdal R-gstat R-maptools R-deldir
BuildRequires:    R-RColorBrewer R-rgdal R-gstat R-maptools R-deldir
%if 0%{with_loop}
Requires:         R-rgeos
BuildRequires:    R-rgeos
%endif
%endif

%description
Classes and methods for spatial data; the classes document where the
spatial location information resides, for 2D or 3D data. Utility functions
are provided, e.g. for plotting data as maps, spatial selection, as well
as methods for retrieving coordinates, for subsetting, print, summary,

%package devel
Summary:          Development files for %{name}
Requires:         %{name}%{?_isa} = %{version}-%{release}
%description devel
Development files for %{name}.


%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_suggests}
%if %{with_loop}
%{_bindir}/R CMD check %{packname}
%else
rm %{packname}/tests/agg.R* %{packname}/tests/over2.R*  # whole file requires rgeos
%{_bindir}/R CMD check %{packname} --no-examples
%endif
%else
rm %{packname}/tests/agg.R* %{packname}/tests/over2.R*  # whole file requires rgeos
_R_CHECK_FORCE_SUGGESTS_=0 \
    %{_bindir}/R CMD check %{packname} --no-examples
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%doc %{rlibdir}/%{packname}/CITATION
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS.Rd
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/data
%{rlibdir}/%{packname}/demo
%{rlibdir}/%{packname}/external
%{rlibdir}/%{packname}/help
%dir %{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/libs/%{packname}.so

%files devel
%{rlibdir}/%{packname}/include


%changelog
* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.5-3
- Split headers into -devel package.

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.5-2
- new package built with tito

* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.5-1
- initial package for Fedora
