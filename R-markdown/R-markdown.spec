%global packname  markdown
%global rlibdir  %{_libdir}/R/library

# Vignettes require knitr, but it requires this package.
%global with_doc  0

Name:             R-%{packname}
Version:          0.8
Release:          2%{?dist}
Summary:          Markdown Rendering for R

License:          GPLv2 and ISC
URL:              https://cran.r-project.org/web/packages/%{packname}/index.html
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{version}.tar.gz
# https://github.com/rstudio/markdown/issues/83
Patch0001:        23a6442c61b4bffe43510fa4105732fab7a0c3e6.patch

# Here's the R view of the dependencies world:
# Depends:
# Imports:   R-utils R-mime
# Suggests:  R-knitr R-RCurl
# LinkingTo:
# Enhances:

Requires:         R-utils R-mime
Suggests:         R-knitr R-RCurl
BuildRequires:    R-devel tex(latex)
BuildRequires:    R-utils R-mime
%if %{with_doc}
BuildRequires:    R-knitr
%endif

# This doesn't exist in Fedora and is basically dead upstream.
Provides:         bundled(sundown) = 07d0d98cec0df93e07debbcf562cac9eaca998f8

%description
Provides R bindings to the Sundown Markdown rendering library. Markdown is a
plain-text formatting syntax that can be converted to 'XHTML' or other formats.

%package devel
Summary:          Development files for %{name}
Requires:         %{name}%{?_isa} = %{version}-%{release}
%description devel
Development files for %{name}.


%prep
%setup -q -c -n %{packname}
pushd %{packname}
%patch0001 -p1
popd

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{with_doc}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname} --ignore-vignettes
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/examples
%doc %{rlibdir}/%{packname}/html
%license %{rlibdir}/%{packname}/COPYING
%license %{rlibdir}/%{packname}/NOTICE
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/resources

%files devel
%{rlibdir}/%{packname}/include


%changelog
* Fri Aug 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8-2
- Patch tests to not use the network.
- Shorten description a bit.

* Fri Aug 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8-1
- initial package for Fedora
